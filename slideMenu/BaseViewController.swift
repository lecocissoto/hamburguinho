//
//  BaseViewController.swift
//  slideMenu
//
//  Created by QuaddroAdm on 18/12/15.
//  Copyright © 2015 QuaddroAdm. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    //MARK: Helpers
    //Add the button to the menu
    func addSlideMenuButton(){
        
        //Create the button and the button target
        let btnShowMenu = UIButton(type: UIButtonType.System)
        btnShowMenu.setImage(defaultMenuImage(), forState: UIControlState.Normal)
        btnShowMenu.frame = CGRectMake(0, 0, 30, 30)
        btnShowMenu.addTarget(self, action: "onSlideMenuButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //Creating a navigation button item
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem
    }
    
    
    //Draw the button image
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        struct Static {
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken, { () -> Void in
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 22), false, 0.0)
            
            UIColor.blackColor().setFill()
            UIBezierPath(rect: CGRectMake(0, 3, 30, 1)).fill()
            UIBezierPath(rect: CGRectMake(0, 10, 30, 1)).fill()
            UIBezierPath(rect: CGRectMake(0, 17, 30, 1)).fill()
            
            UIColor.whiteColor().setFill()
            UIBezierPath(rect: CGRectMake(0, 4, 30, 1)).fill()
            UIBezierPath(rect: CGRectMake(0, 11,  30, 1)).fill()
            UIBezierPath(rect: CGRectMake(0, 18, 30, 1)).fill()
            
            defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
        })
        
        return defaultMenuImage;
    }
    
    
    //MARK: Actions
    //Slide the menu
    func onSlideMenuButtonPressed(sender: UIButton){
        
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.mainScreen().bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clearColor()
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.enabled = true
        sender.tag = 10
        
        let menuVC = self.storyboard!.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        
        menuVC.view.layoutIfNeeded()
        menuVC.view.backgroundColor = UIColor.redColor()
        menuVC.view.frame = CGRectMake(0 - UIScreen.mainScreen().bounds.size.width, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
        UIView.animateWithDuration(0.3) { () -> Void in
            menuVC.view.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
            sender.enabled = true
        }
        
    }
    
    //MARK: SlideMenuDelegate
    func slideMenuItemSelectedAtIndex(index: Int32) {
        let topViewController: UIViewController = self.navigationController!.topViewController!
        print("ViewControlle é \(topViewController)")
    }
    
    
}
