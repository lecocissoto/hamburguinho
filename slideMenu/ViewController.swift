//
//  ViewController.swift
//  slideMenu
//
//  Created by QuaddroAdm on 18/12/15.
//  Copyright © 2015 QuaddroAdm. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

