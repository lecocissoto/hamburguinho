//
//  MenuViewController.swift
//  slideMenu
//
//  Created by QuaddroAdm on 18/12/15.
//  Copyright © 2015 QuaddroAdm. All rights reserved.
//

import UIKit

//Declaring the slide menu protocol
protocol SlideMenuDelegate{
    func slideMenuItemSelectedAtIndex(index: Int32)
}


class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var arrayMenuOptions = [Dictionary<String, String>]()
    
    var delegate: SlideMenuDelegate?
    
    var btnMenu:UIButton!
    
    //MARK: Load
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        updateArrayMenuOptions()
    }
    
    //MARK: Helpers
    //Add itens to the menu array
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":"Home", "icon":"HomeIcon"])
        arrayMenuOptions.append(["title":"Play", "icon":"PlayIcon"])
        arrayMenuOptions.append(["title":"Camera", "icon":"CameraIcon"])
        tableView.reloadData()
        print("Atualizou os dados")
    }
    
    //UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(arrayMenuOptions.count)
        return arrayMenuOptions.count
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMenu")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()

        cell.textLabel?.text = arrayMenuOptions[indexPath.row]["title"]
        print("Carregou cell")
        return cell
    }
    
    
    
    
    
}
